characters = {
	#country leaders
	FFF_Zomie_leader_sangshi = {
		name = FFF_Zomie_leader_sangshi
		portraits = {
			civilian = {
				large = "gfx/leaders/FFF/sanshi.tga"
			}
			army = {
				large = "gfx/leaders/FFF/sanshi.tga"
				small = "gfx/leaders/FFF/sanshi_small.tga"
			}
		}
		country_leader = {
			ideology = sangshi
			traits = {
				Zomie_leader
			}
			id = -1
		}
	}
	FFF_Zomie_leader_despotism = {
		name = FFF_Zomie_leader_despotism
		portraits = {
			civilian = {
				large = "gfx/leaders/FFF/sanshi.tga"
			}
			army = {
				large = "gfx/leaders/FFF/sanshi.tga"
				small = "gfx/leaders/FFF/sanshi_small.tga"
			}
		}
		country_leader = {
			ideology = despotism
			traits = {
				Zomie_leader
			}
			id = -1
		}
	}
	FFF_Zomie_leader_leninism = {
		name = FFF_Zomie_leader_leninism
		portraits = {
			civilian = {
				large = "gfx/leaders/FFF/sanshi.tga"
			}
			army = {
				large = "gfx/leaders/FFF/sanshi.tga"
				small = "gfx/leaders/FFF/sanshi_small.tga"
			}
		}
		country_leader = {
			ideology = leninism
			traits = {
				Zomie_leader
			}
			id = -1
		}
	}
	FFF_Zomie_leader_liberalism = {
		name = FFF_Zomie_leader_liberalism
		portraits = {
			civilian = {
				large = "gfx/leaders/FFF/sanshi.tga"
			}
			army = {
				large = "gfx/leaders/FFF/sanshi.tga"
				small = "gfx/leaders/FFF/sanshi_small.tga"
			}
		}
		country_leader = {
			ideology = liberalism
			traits = {
				Zomie_leader
			}
			id = -1
		}
	}
	FFF_Zomie_leader_nazism = {
		name = FFF_Zomie_leader_nazism
		portraits = {
			civilian = {
				large = "gfx/leaders/FFF/sanshi.tga"
			}
			army = {
				large = "gfx/leaders/FFF/sanshi.tga"
				small = "gfx/leaders/FFF/sanshi_small.tga"
			}
		}
		country_leader = {
			ideology = nazism
			traits = {
				Zomie_leader
			}
			id = -1
		}
	}
	#advisors
	FFF_ZomieAdvisor_Construction = {
		name = FFF_ZomieAdvisor_Construction
		portraits = {
			army = {
				large = "gfx/leaders/FFF/sanshi.tga"
				small = "gfx/leaders/FFF/sanshi_small.tga"
			}
		}
		advisor = {
			slot = political_advisor
			idea_token = ZomieAdvisor_Construction
			cost = 0
			allowed = {
				original_tag = FFF
			}
			available = {
			}
			traits = {
				ZomieAdvisor_Construction_Advisor
			}
			ai_will_do = {
				factor = 10
			}
		}
	}
	FFF_ZomieAdvisor_Production = {
		name = FFF_ZomieAdvisor_Production
		portraits = {
			army = {
				large = "gfx/leaders/FFF/sanshi.tga"
				small = "gfx/leaders/FFF/sanshi_small.tga"
			}
		}
		advisor = {
			slot = political_advisor
			idea_token = ZomieAdvisor_Production
			cost = 0
			allowed = {
				original_tag = FFF
			}
			available = {
			}
			traits = {
				ZomieAdvisor_Production_Advisor
			}
			ai_will_do = {
				factor = 10
			}
		}
	}
	#army
	FFF_ZomieAdvisor_Army = {
		name = FFF_ZomieAdvisor_Army
		portraits = {
			army = {
				large = "gfx/leaders/FFF/sanshi.tga"
				small = "gfx/leaders/FFF/sanshi_small.tga"
			}
		}
		advisor = {
			slot = political_advisor
			idea_token = FFF_ZomieAdvisor_Army
			cost = 0
			allowed = {
				original_tag = FFF
			}
			available = {
				has_country_flag = FFF_Infantry_AP
			}
			traits = {
				ZomieAdvisor_Army
			}
		}
	}
	#infantry
	FFF_ZomieAdvisor_InfantryChief = {
		name = FFF_ZomieAdvisor_InfantryChief
		portraits = {
			army = {
				large = "gfx/leaders/FFF/sanshi.tga"
				small = "gfx/leaders/FFF/sanshi_small.tga"
			}
		}
		advisor = {
			slot = army_chief
			idea_token = FFF_ZomieAdvisor_InfantryChief
			cost = 0
			allowed = {
				original_tag = FFF
			}
			available = {
				has_country_flag = FFF_Infantry_AP
			}
			traits = {
				ZomieInfantry_Chief
			}
		}
	}
	FFF_InfantryHighCommand_Infanrty = {
		name = FFF_InfantryHighCommand_Infanrty
		portraits = {
			army = {
				large = "gfx/leaders/FFF/sanshi.tga"
				small = "gfx/leaders/FFF/sanshi_small.tga"
			}
		}
		advisor = {
			slot = high_command
			idea_token = FFF_InfantryHiCommand_Infanrty
			cost = 0
			allowed = {
				original_tag = FFF
			}
			available = {
			}
			traits = {
				InfantryHighCommand_Infanrty
			}
		}
	}
	FFF_InfantryHighCommand_Xp = {
		name = FFF_InfantryHighCommand_Xp
		portraits = {
			army = {
				small = "gfx/leaders/FFF/sanshi_small.tga"
			}
		}
		advisor = {
			slot = high_command
			idea_token = FFF_InfantryHighCommand_Xp
			cost = 0
			allowed = {
				original_tag = FFF
			}
			available = {
			}
			traits = {
				InfantryHighCommand_Xp
			}
		}
	}
	FFF_InfantryHighCommand_AP = {
		name = FFF_InfantryHighCommand_AP
		portraits = {
			army = {
				small = "gfx/leaders/FFF/sanshi_small.tga"
			}
		}
		advisor = {
			slot = high_command
			idea_token = FFF_InfantryHighCommand_AP
			cost = 0
			allowed = {
				original_tag = FFF
			}
			available = {
			}
			traits = {
				InfantryHighCommand_AP
			}
		}
	}
}
