﻿###########################
# FFF Events
###########################
add_namespace = fff
# 说明
country_event = {
	id = fff.1
	title = fff.1.t
	desc = fff.1.d
	picture = GFX_report_event_generic_peaceful_annexation
	is_triggered_only = no
	fire_only_once = yes
	trigger = {
		is_ai = no
	}
	option = {
		name = fff.1.a
	}
}

#丧尸危机在南美爆发爆发
country_event = {
	id = fff.2
	title = fff.2.t
	desc = fff.2.d
	picture = GFX_sangshixiji
	is_triggered_only = yes
	immediate = {
		set_global_flag = sangshi_manyan
	}
	option = {
		name = fff.2.a
		FFF = {
			declare_war_on = {
				target = BRA
				type = annex_everything
			}
		}
		FFF = {
			declare_war_on = {
				target = PRU
				type = annex_everything
			}
		}
		FFF = {
			declare_war_on = {
				target = CHL
				type = annex_everything
			}
		}
		FFF = {
			declare_war_on = {
				target = URG
				type = annex_everything
			}
		}
		FFF = {
			declare_war_on = {
				target = ARG
				type = annex_everything
			}
		}
		FFF = {
			declare_war_on = {
				target = PAR
				type = annex_everything
			}
		}
		FFF = {
			transfer_state = 488
		}
		FFF = {
			transfer_state = 486
		}
		FFF = {
			transfer_state = 307
		}
		FFF = {
			transfer_state = 489
		}
		FFF = {
			transfer_state = 306
		}
		FFF = {
			transfer_state = 687
		}
		FFF = {
			transfer_state = 309
		}
		FFF = {
			transfer_state = 310
		}
		FFF = {
			transfer_state = 493
		}
		FFF = {
			transfer_state = 305
		}
		FFF = {
			transfer_state = 490
		}
		FFF = {
			transfer_state = 487
		}
		FFF = {
			transfer_state = 506
		}
		FFF = {
			transfer_state = 302
		}
		FFF = {
			transfer_state = 497
		}
		FFF = {
			transfer_state = 505
		}
		FFF = {
			transfer_state = 280
		}
		news_event = {
			hours = 6
			id = sss.1
		}
	}
}

#巴西总统拜访美国
country_event = {
	id = fff.3
	title = fff.3.t
	desc = fff.3.d
	picture = GFX_report_event_romania_parliament
	is_triggered_only = no
	fire_only_once = yes
	trigger = {
		tag = USA
		BRA = {
			has_war_with = FFF
		}
	}
	mean_time_to_happen = {
		days = 10
	}
	option = {
		ai_chance = {
			factor = 5
		}
		USA = {
			add_to_war = {
				enemy = FFF
				targeted_alliance = BRA
				hostility_reason = asked_to_join
			}
		}
		name = fff.3.a
		add_manpower = 1000000
		news_event = {
			hours = 6
			id = sss.2
		}
	}
	option = {
		ai_chance = {
			factor = 95
		}
		name = fff.3.b
		add_political_power = 200
		news_event = {
			hours = 6
			id = sss.2
		}
	}
}

#丧尸蔓延到南桦太
country_event = {
	id = fff.4
	title = fff.4.t
	desc = fff.4.d
	picture = GFX_sangshixiji
	is_triggered_only = yes
	mean_time_to_happen = {
		days = 0
	}
	immediate = {
		set_global_flag = sangshi_riben
	}
	option = {
		name = fff.4.a
		FFF = {
			transfer_state = 637
		}
		FFF = {
			transfer_state = 655
		}
		FFF = {
			transfer_state = 537
		}
		FFF = {
			transfer_state = 555
		}
		FFF = {
			transfer_state = 536
		}
		news_event = {
			hours = 6
			id = sss.3
		}
		FFF = {
			country_event = fff.5
		}
	}
}

#同类增加
country_event = {
	id = fff.5
	title = fff.5.t
	desc = fff.5.d
	picture = GFX_report_event_chinese_soldiers_01
	is_triggered_only = yes
	option = {
		name = fff.5.a
		FFF = {
			hidden_effect = {
				load_oob = "FFF_jap"
			}
			custom_effect_tooltip = FFF_jap
		}
		SOV = {
			country_event = {
				days = 3
				id = fff.6
			}
		}
	}
}

#关于远东丧尸
country_event = {
	id = fff.6
	title = fff.6.t
	desc = fff.6.d
	picture = GFX_report_event_worker_protests
	is_triggered_only = yes
	option = {
		name = fff.6.a
		ai_chance = {
			factor = 90
		}
		add_manpower = 10000
		add_stability = 0.1
		news_event = {
			hours = 6
			id = sss.4
		}
	}
	option = {
		name = fff.6.b
		ai_chance = {
			factor = 10
		}
		add_manpower = -10000
		add_war_support = 0.2
		add_stability = -0.2
		SOV = {
			transfer_state = 637
		}
		SOV = {
			transfer_state = 655
		}
		SOV = {
			transfer_state = 537
		}
		news_event = {
			hours = 6
			id = sss.5
		}
	}
}

#南美洲陷落
country_event = {
	id = fff.7
	title = fff.7.t
	desc = fff.7.d
	picture = GFX_report_event_long_march
	is_triggered_only = no
	fire_only_once = yes
	trigger = {
		tag = FFF
		BRA = {
			exists = no
		}
		ARG = {
			exists = no
		}
		PAR = {
			exists = no
		}
		PRU = {
			exists = no
		}
		CHL = {
			exists = no
		}
		URG = {
			exists = no
		}
	}
	mean_time_to_happen = {
		days = 0
	}
	immediate = {
		set_global_flag = sangshi_nanmeixianluo		#南美洲沦陷
	}
	option = {
		name = fff.7.a
		hidden_effect = {
			FRA = {
				country_event = {
					days = 5
					id = ppp.1
				}
			}
			ENG = {
				country_event = {
					days = 5
					id = ppp.1
				}
			}
			GER = {
				country_event = {
					days = 5
					id = ppp.1
				}
			}
			ITA = {
				country_event = {
					days = 5
					id = ppp.1
				}
			}
			USA = {
				country_event = {
					days = 5
					id = ppp.1
				}
			}
			SOV = {
				country_event = {
					days = 5
					id = ppp.1
				}
			}
			news_event = {
				hours = 6
				id = sss.6
			}
			news_event = {
				hours = 200
				id = sss.7
			}
		}
	}
}

#丧尸蔓延到北美
country_event = {
	id = fff.8
	title = fff.8.t
	desc = fff.8.d
	picture = GFX_sangshixiji
	is_triggered_only = yes
	option = {
		name = fff.8.a
		FFF = {
			transfer_state = 304
		}
		FFF = {
			transfer_state = 685
		}
		FFF = {
			transfer_state = 316
		}
		FFF = {
			transfer_state = 317
		}
		FFF = {
			transfer_state = 312
		}
		FFF = {
			transfer_state = 314
		}
		FFF = {
			transfer_state = 313
		}
		FFF = {
			transfer_state = 311
		}
		FFF = {
			transfer_state = 475
		}
		FFF = {
			transfer_state = 474
		}
		FFF = {
			declare_war_on = {
				target = MEX
				type = annex_everything
			}
		}
		country_event = {
			days = 1
			id = fff.9
		}
		news_event = {
			hours = 6
			id = sss.8
		}
		USA = {
			country_event = {
				days = 5
				id = ppp.2
			}
		}
	}
}

#同类增加
country_event = {
	id = fff.9
	title = fff.9.t
	desc = fff.9.d
	picture = GFX_report_event_chinese_soldiers_01
	is_triggered_only = yes
	option = {
		name = fff.9.a
		FFF = {
			hidden_effect = {
				load_oob = "FFF_mex"
			}
			custom_effect_tooltip = FFF_mex
		}
	}
}

#丧尸蔓延到刚果
country_event = {
	id = fff.10
	title = fff.10.t
	desc = fff.10.d
	picture = GFX_sangshixiji
	is_triggered_only = yes
	option = {
		name = fff.10.a
		FFF = {
			transfer_state = 295
		}
		FFF = {
			transfer_state = 538
		}
		FFF = {
			transfer_state = 718
		}
		country_event = {
			days = 1
			id = fff.11
		}
		POR = {
			country_event = {
				days = 5
				id = ppp.3
			}
		}
		FRA = {
			country_event = {
				days = 5
				id = ppp.48
			}
		}
		ENG = {
			country_event = {
				days = 5
				id = ppp.49
			}
		}
	}
}

#同类增加
country_event = {
	id = fff.11
	title = fff.11.t
	desc = fff.11.d
	picture = GFX_sangshixiji
	is_triggered_only = yes
	option = {
		name = fff.11.a
		FFF = {
			hidden_effect = {
				load_oob = "FFF_bls"
			}
			custom_effect_tooltip = FFF_bls
		}
	}
}

#同类增加
country_event = {
	id = fff.12
	title = fff.12.t
	desc = fff.12.d
	picture = GFX_sangshixiji
	is_triggered_only = yes
	option = {
		name = fff.12.a
		GGG = {
			hidden_effect = {
				load_oob = "FFF_pty"
			}
			custom_effect_tooltip = FFF_pty
		}
	}
}

#丧尸袭击日本
country_event = {
	id = fff.13
	title = fff.13.t
	desc = fff.13.d
	picture = GFX_sangshixiji
	is_triggered_only = yes
	option = {
		name = fff.13.a
		country_event = {
			days = 1
			id = fff.14
		}
		news_event = {
			hours = 6
			id = sss.11
		}
		FFF = {
			declare_war_on = {
				target = JAP
				type = annex_everything
			}
		}
	}
}

#同类增加
country_event = {
	id = fff.14
	title = fff.14.t
	desc = fff.14.d
	picture = GFX_sangshixiji
	is_triggered_only = yes
	option = {
		name = fff.14.a
		FFF = {
			hidden_effect = {
				load_oob = "FFF_jap"
			}
			custom_effect_tooltip = FFF_jap
		}
	}
}

#丧尸蔓延中东
country_event = {
	id = fff.15
	title = fff.15.t
	desc = fff.15.d
	picture = GFX_sangshixiji
	is_triggered_only = yes
	option = {
		name = fff.15.a
		FFF = {
			annex_country = {
				target = YEM
				transfer_troops = no
			}
		}
		FFF = {
			annex_country = {
				target = SAU
				transfer_state = no
			}
		}
		FFF = {
			annex_country = {
				target = OMA
				transfer_state = no
			}
		}
		FFF = {
			annex_country = {
				target = IRQ
				transfer_state = no
			}
		}
		FFF = {
			annex_country = {
				target = PER
				transfer_troops = no
			}
		}
		FFF = {
			transfer_state = 765
			transfer_state = 658
			transfer_state = 656
		}
		country_event = {
			days = 1
			id = fff.16
		}
		news_event = {
			hours = 6
			id = sss.12
		}
	}
}

#同类增加
country_event = {
	id = fff.16
	title = fff.16.t
	desc = fff.16.d
	picture = GFX_sangshixiji
	is_triggered_only = yes
	option = {
		name = fff.16.a
		FFF = {
			hidden_effect = {
				load_oob = "FFF_per"
			}
			custom_effect_tooltip = FFF_per
		}
	}
}

#丧尸袭击美国
country_event = {
	id = fff.17
	title = fff.17.t
	desc = fff.17.d
	picture = GFX_sangshixiji
	is_triggered_only = yes
	option = {
		name = fff.17.a
		FFF = {
			declare_war_on = {
				target = USA
				type = annex_everything
			}
		}
		news_event = {
			hours = 6
			id = sss.13
		}
	}
}

#丧尸袭击苏联
country_event = {
	id = fff.18
	title = fff.18.t
	desc = fff.18.d
	picture = GFX_sangshixiji
	is_triggered_only = yes
	option = {
		name = fff.18.a
		FFF = {
			declare_war_on = {
				target = SOV
				type = annex_everything
			}
		}
		news_event = {
			hours = 6
			id = sss.14
		}
	}
}

#丧尸袭击欧洲
country_event = {
	id = fff.19
	title = fff.19.t
	desc = fff.19.d
	picture = GFX_sangshixiji
	is_triggered_only = yes
	option = {
		name = fff.19.az
		if = {
			limit = {
				D01 = {
					exists = yes
				}
			}
			FFF = {
				declare_war_on = {
					target = D01
					type = annex_everything
				}
			}
		}
		if = {
			limit = {
				D02  = {
					exists = yes
				}
			}
			FFF = {
				declare_war_on = {
					target = D02
					type = annex_everything
				}
			}
		}
		FFF = {
			declare_war_on = {
				target = SPR
				type = annex_everything
			}
		}
		FFF = {
			declare_war_on = {
				target = POR
				type = annex_everything
			}
		}
		FFF = {
			declare_war_on = {
				target = ENG
				type = annex_everything
			}
		}
		FFF = {
			declare_war_on = {
				target = FRA
				type = annex_everything
			}
		}
		FFF = {
			declare_war_on = {
				target = BEL
				type = annex_everything
			}
		}
		FFF = {
			declare_war_on = {
				target = HOL
				type = annex_everything
			}
		}
		FFF = {
			declare_war_on = {
				target = IRE
				type = annex_everything
			}
		}
		FFF = {
			hidden_effect = {
				load_oob = "FFF_pty"
			}
			custom_effect_tooltip = FFF_pty
		}
		news_event = {
			hours = 6
			id = sss.15
		}
	}
}

#丧尸袭击欧洲
country_event = {
	id = fff.20
	title = fff.20.t
	desc = fff.20.d
	picture = GFX_sangshixiji
	is_triggered_only = yes
	option = {
		name = fff.20.a
		FFF = {
			declare_war_on = {
				target = GER
				type = annex_everything
			}
		}
		FFF = {
			declare_war_on = {
				target = ITA
				type = annex_everything
			}
		}
	}
}

#葡萄牙
country_event = {
	id = fff.21
	title = fff.21.t
	desc = fff.21.d
	picture = GFX_sangshixiji
	is_triggered_only = yes
	option = {
		name = fff.21.a
		FFF = {
			annex_country = {
				target = POR
				transfer_troops = no
			}
		}
	}
}

#苏格兰高地有丧尸体感染
country_event = {
	id = fff.22
	title = fff.22.t
	desc = fff.22.d
	picture = GFX_report_event_pierre_laval
	is_triggered_only = yes
	option = {
		name = fff.22.a
		FFF = {
			transfer_state = 120
		}
		FFF = {
			hidden_effect = {
				load_oob = "FFF_SGL"
			}
			custom_effect_tooltip = FFF_SGL
		}
	}
}

#布雷斯特感染
country_event = {
	id = fff.23
	title = fff.23.t
	desc = fff.23.d
	picture = GFX_report_event_pierre_laval
	is_triggered_only = yes
	option = {
		name = fff.23.a
		FFF = {
			transfer_state = 14
		}
		FFF = {
			hidden_effect = {
				load_oob = "FFF_blst"
			}
			custom_effect_tooltip = FFF_blst
		}
		news_event = {
			hours = 6
			id = sss.37
		}
	}
}

#丹麦受染
country_event = {
	id = fff.24
	title = fff.24.t
	desc = fff.24.d
	picture = GFX_report_event_pierre_laval
	is_triggered_only = yes
	option = {
		name = fff.24.a
		FFF = {
			transfer_state = 99
		}
		FFF = {
			hidden_effect = {
				load_oob = "FFF_danmai"
			}
			custom_effect_tooltip = FFF_danmai
		}
		FFF = {
			declare_war_on = {
				target = NOR
				type = annex_everything
			}
		}
		FFF = {
			declare_war_on = {
				target = DEN
				type = annex_everything
			}
		}
		FFF = {
			declare_war_on = {
				target = SWE
				type = annex_everything
			}
		}
		news_event = {
			hours = 6
			id = sss.38
		}
	}
}

#沃尔霍夫感染
country_event = {
	id = fff.25
	title = fff.25.t
	desc = fff.25.d
	picture = GFX_report_event_pierre_laval
	is_triggered_only = yes
	option = {
		name = fff.25.a
		FFF = {
			transfer_state = 244
		}
		FFF = {
			hidden_effect = {
				load_oob = "FFF_wehf"
			}
			custom_effect_tooltip = FFF_wehf
		}
	}
}
