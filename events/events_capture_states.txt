﻿#########################
## qqq Mod Events ##
#########################
add_namespace = qqq
#东京被占领
news_event = {
	id = qqq.1
	title = qqq.1.t
	desc = qqq.1.d
	picture = GFX_news_event_6666
	major = yes
	trigger = {
		282 = {
			is_controlled_by = FFF
		}
		282 = {
			is_owned_by = JAP
		}
		FFF = {
			has_war_with = JAP
		}
		NOT = {
			has_global_flag = djxl
		}
	}
	immediate = {
		set_global_flag = djxl
	}
	mean_time_to_happen = {
		days = 2
	}
	option = {
		name = qqq.1.a
		trigger = {
			NOT = {
				TAG = FFF
				TAG = JAP
			}
		}
	}
	option = {
		name = qqq.1.b
		trigger = {
			TAG = FFF
		}
	}
	option = {
		name = qqq.1.c
		trigger = {
			TAG = JAP
		}
	}
}

#南京被占领
news_event = {
	id = qqq.2
	title = qqq.2.t
	desc = qqq.2.d
	picture = GFX_news_event_7777
	major = yes
	trigger = {
		613 = {
			is_controlled_by = FFF
		}
		613 = {
			is_owned_by = CHI
		}
		FFF = {
			has_war_with = CHI
		}
		NOT = {
			has_global_flag = www
		}
	}
	immediate = {
		set_global_flag = www
	}
	mean_time_to_happen = {
		days = 2
	}
	option = {
		name = qqq.2.a
		trigger = {
			NOT = {
				TAG = FFF
				TAG = CHI
			}
		}
	}
	option = {
		name = qqq.2.b
		trigger = {
			TAG = FFF
		}
	}
	option = {
		name = qqq.2.c
		trigger = {
			TAG = CHI
		}
	}
}

#北京被占领
news_event = {
	id = qqq.3
	title = qqq.3.t
	desc = qqq.3.d
	picture = GFX_news_event_7777
	major = yes
	trigger = {
		608 = {
			is_controlled_by = FFF
		}
		608 = {
			is_owned_by = CHI
		}
		FFF = {
			has_war_with = CHI
		}
		NOT = {
			has_global_flag = bj
		}
	}
	immediate = {
		set_global_flag = bj
	}
	mean_time_to_happen = {
		days = 2
	}
	option = {
		name = qqq.3.a
		trigger = {
			NOT = {
				TAG = FFF
				TAG = CHI
			}
		}
	}
	option = {
		name = qqq.3.b
		trigger = {
			TAG = FFF
		}
	}
	option = {
		name = qqq.3.c
		trigger = {
			TAG = CHI
		}
	}
}

#莫斯科被占领
news_event = {
	id = qqq.4
	title = qqq.4.t
	desc = qqq.4.d
	picture = GFX_news_event_8888
	major = yes
	trigger = {
		219 = {
			is_controlled_by = FFF
		}
		219 = {
			is_owned_by = SOV
		}
		FFF = {
			has_war_with = SOV
		}
		NOT = {
			has_global_flag = msk
		}
	}
	immediate = {
		set_global_flag = msk
	}
	mean_time_to_happen = {
		days = 2
	}
	option = {
		name = qqq.4.a
		trigger = {
			NOT = {
				TAG = FFF
				TAG = SOV
			}
		}
	}
	option = {
		name = qqq.4.b
		trigger = {
			TAG = FFF
		}
	}
	option = {
		name = qqq.4.c
		trigger = {
			TAG = SOV
		}
	}
}

#柏林被占领
news_event = {
	id = qqq.6
	title = qqq.6.t
	desc = qqq.6.d
	picture = GFX_news_event_9999
	major = yes
	trigger = {
		64 = {
			is_controlled_by = FFF
		}
		64 = {
			is_owned_by = GER
		}
		FFF = {
			has_war_with = GER
		}
		NOT = {
			has_global_flag = bl
		}
	}
	immediate = {
		set_global_flag = bl
	}
	mean_time_to_happen = {
		days = 2
	}
	option = {
		name = qqq.6.a
		trigger = {
			NOT = {
				TAG = FFF
				TAG = SOV
			}
		}
	}
	option = {
		name = qqq.6.b
		trigger = {
			TAG = FFF
		}
	}
	option = {
		name = qqq.6.c
		trigger = {
			TAG = GER
		}
	}
}

#巴黎被占领
news_event = {
	id = qqq.7
	title = qqq.7.t
	desc = qqq.7.d
	picture = GFX_news_event_6666
	major = yes
	trigger = {
		16 = {
			is_controlled_by = FFF
		}
		16 = {
			is_owned_by = FRA
		}
		FFF = {
			has_war_with = FRA
		}
		NOT = {
			has_global_flag = bll
		}
	}
	immediate = {
		set_global_flag = bll
	}
	mean_time_to_happen = {
		days = 2
	}
	option = {
		name = qqq.7.a
		trigger = {
			NOT = {
				TAG = FFF
				TAG = FRA
			}
		}
	}
	option = {
		name = qqq.7.b
		trigger = {
			TAG = FFF
		}
	}
	option = {
		name = qqq.7.c
		trigger = {
			TAG = FRA
		}
	}
}

#伦敦被占领
news_event = {
	id = qqq.8
	title = qqq.8.t
	desc = qqq.8.d
	picture = GFX_news_event_6666
	major = yes
	trigger = {
		126 = {
			is_controlled_by = FFF
		}
		126 = {
			is_owned_by = ENG
		}
		FFF = {
			has_war_with = ENG
		}
		NOT = {
			has_global_flag = ld
		}
	}
	immediate = {
		set_global_flag = ld
	}
	mean_time_to_happen = {
		days = 2
	}
	option = {
		name = qqq.8.a
		trigger = {
			NOT = {
				TAG = FFF
				TAG = ENG
			}
		}
	}
	option = {
		name = qqq.8.b
		trigger = {
			TAG = FFF
		}
	}
	option = {
		name = qqq.8.c
		trigger = {
			TAG = ENG
		}
	}
}

#华盛顿被占领
news_event = {
	id = qqq.9
	title = qqq.9.t
	desc = qqq.9.d
	picture = GFX_news_event_999
	major = yes
	trigger = {
		361 = {
			is_controlled_by = FFF
		}
		361 = {
			is_owned_by = USA
		}
		FFF = {
			has_war_with = USA
		}
		NOT = {
			has_global_flag = hsd
		}
	}
	immediate = {
		set_global_flag = hsd
	}
	mean_time_to_happen = {
		days = 2
	}
	option = {
		name = qqq.9.a
		trigger = {
			NOT = {
				TAG = FFF
				TAG = USA
			}
		}
	}
	option = {
		name = qqq.9.b
		trigger = {
			TAG = FFF
		}
	}
	option = {
		name = qqq.9.c
		trigger = {
			TAG = USA
		}
	}
}

#罗马被占领
news_event = {
	id = qqq.10
	title = qqq.10.t
	desc = qqq.10.d
	picture = GFX_news_event_053
	major = yes
	trigger = {
		2 = {
			is_controlled_by = FFF
		}
		2 = {
			is_owned_by = ITA
		}
		FFF = {
			has_war_with = ITA
		}
		NOT = {
			has_global_flag = lm
		}
	}
	immediate = {
		set_global_flag = lm
	}
	mean_time_to_happen = {
		days = 2
	}
	option = {
		name = qqq.10.a
		trigger = {
			NOT = {
				TAG = FFF
				TAG = ITA
			}
		}
	}
	option = {
		name = qqq.10.b
		trigger = {
			TAG = FFF
		}
	}
	option = {
		name = qqq.10.c
		trigger = {
			TAG = ITA
		}
	}
}

#丧尸亚马逊被占领-USA
news_event = {
	id = qqq.11
	title = qqq.11.t
	desc = qqq.11.d
	picture = GFX_news_event_99999
	major = yes
	trigger = {
		495 = {
			is_controlled_by = USA
		}
		495 = {
			is_owned_by = FFF
		}
		USA = {
			has_war_with = FFF
		}
		NOT = {
			has_global_flag = ymx
		}
	}
	immediate = {
		set_global_flag = ymx
	}
	mean_time_to_happen = {
		days = 2
	}
	option = {
		name = qqq.11.a
		trigger = {
			NOT = {
				TAG = USA
				TAG = FFF
			}
		}
	}
	option = {
		name = qqq.11.b
		trigger = {
			TAG = USA
		}
	}
	option = {
		name = qqq.11.c
		trigger = {
			TAG = FFF
		}
	}
}

#丧尸亚马逊被占领-ENG
news_event = {
	id = qqq.12
	title = qqq.12.t
	desc = qqq.12.d
	picture = GFX_news_event_99999
	major = yes
	trigger = {
		495 = {
			is_controlled_by = ENG
		}
		495 = {
			is_owned_by = FFF
		}
		ENG = {
			has_war_with = FFF
		}
		NOT = {
			has_global_flag = ymx
		}
	}
	immediate = {
		set_global_flag = ymx
	}
	mean_time_to_happen = {
		days = 2
	}
	option = {
		name = qqq.11.a
		trigger = {
			NOT = {
				TAG = ENG
				TAG = FFF
			}
		}
	}
	option = {
		name = qqq.11.b
		trigger = {
			TAG = ENG
		}
	}
	option = {
		name = qqq.11.c
		trigger = {
			TAG = FFF
		}
	}
}

#丧尸亚马逊被占领-GER
news_event = {
	id = qqq.13
	title = qqq.13.t
	desc = qqq.13.d
	picture = GFX_news_event_99999
	major = yes
	trigger = {
		495 = {
			is_controlled_by = GER
		}
		495 = {
			is_owned_by = FFF
		}
		GER = {
			has_war_with = FFF
		}
		NOT = {
			has_global_flag = ymx
		}
	}
	immediate = {
		set_global_flag = ymx
	}
	mean_time_to_happen = {
		days = 2
	}
	option = {
		name = qqq.11.a
		trigger = {
			NOT = {
				TAG = GER
				TAG = FFF
			}
		}
	}
	option = {
		name = qqq.11.b
		trigger = {
			TAG = GER
		}
	}
	option = {
		name = qqq.11.c
		trigger = {
			TAG = FFF
		}
	}
}

#丧尸亚马逊被占领-SOV
news_event = {
	id = qqq.14
	title = qqq.14.t
	desc = qqq.14.d
	picture = GFX_news_event_99999
	major = yes
	trigger = {
		495 = {
			is_controlled_by = SOV
		}
		495 = {
			is_owned_by = FFF
		}
		SOV = {
			has_war_with = FFF
		}
		NOT = {
			has_global_flag = ymx
		}
	}
	immediate = {
		set_global_flag = ymx
	}
	mean_time_to_happen = {
		days = 2
	}
	option = {
		name = qqq.11.a
		trigger = {
			NOT = {
				TAG = SOV
				TAG = FFF
			}
		}
	}
	option = {
		name = qqq.11.b
		trigger = {
			TAG = SOV
		}
	}
	option = {
		name = qqq.11.c
		trigger = {
			TAG = FFF
		}
	}
}

#丧尸亚马逊被占领-FRA
news_event = {
	id = qqq.15
	title = qqq.15.t
	desc = qqq.15.d
	picture = GFX_news_event_99999
	major = yes
	trigger = {
		495 = {
			is_controlled_by = FRA
		}
		495 = {
			is_owned_by = FFF
		}
		FRA = {
			has_war_with = FFF
		}
		NOT = {
			has_global_flag = ymx
		}
	}
	immediate = {
		set_global_flag = ymx
	}
	mean_time_to_happen = {
		days = 2
	}
	option = {
		name = qqq.11.a
		trigger = {
			NOT = {
				TAG = FRA
				TAG = FFF
			}
		}
	}
	option = {
		name = qqq.11.b
		trigger = {
			TAG = FRA
		}
	}
	option = {
		name = qqq.11.c
		trigger = {
			TAG = FFF
		}
	}
}

#丧尸亚马逊被占领-JAP
news_event = {
	id = qqq.16
	title = qqq.16.t
	desc = qqq.16.d
	picture = GFX_news_event_99999
	major = yes
	trigger = {
		495 = {
			is_controlled_by = JAP
		}
		495 = {
			is_owned_by = FFF
		}
		JAP = {
			has_war_with = FFF
		}
		NOT = {
			has_global_flag = ymx
		}
	}
	immediate = {
		set_global_flag = ymx
	}
	mean_time_to_happen = {
		days = 2
	}
	option = {
		name = qqq.11.a
		trigger = {
			NOT = {
				TAG = JAP
				TAG = FFF
			}
		}
	}
	option = {
		name = qqq.11.b
		trigger = {
			TAG = JAP
		}
	}
	option = {
		name = qqq.11.c
		trigger = {
			TAG = FFF
		}
	}
}

#解放巴黎-rlei
news_event = {
	id = qqq.17
	title = qqq.17.t
	desc = qqq.17.d
	picture = GFX_news_event_99999
	major = yes
	trigger = {
		16 = {
			is_controlled_by = JAP
		}
		16 = {
			is_owned_by = FFF
		}
		JAP = {
			has_war_with = FFF
		}
		NOT = {
			has_global_flag = ymx
		}
	}
	immediate = {
		set_global_flag = ymx
	}
	mean_time_to_happen = {
		days = 2
	}
	option = {
		name = qqq.11.a
		trigger = {
			NOT = {
				TAG = JAP
				TAG = FFF
			}
		}
	}
	option = {
		name = qqq.11.b
		trigger = {
			TAG = JAP
		}
	}
	option = {
		name = qqq.11.c
		trigger = {
			TAG = FFF
		}
	}
}
