﻿###########################
# ppp Events
###########################
add_namespace = ppp
#列强的反应
country_event = {
	id = ppp.1
	title = ppp.1.t
	desc = ppp.1.d
	picture = GFX_report_event_pla_in_beijing
	is_triggered_only = yes
	option = {
		name = ppp.1.a
		ai_chance = {
			factor = 50
		}
		add_ideas = jianzaoweiqiang
	}
	option = {
		name = ppp.1.b
		ai_chance = {
			factor = 50
		}
		add_ideas = tezhonbudui
	}
}

#美国的反应
country_event = {
	id = ppp.2
	title = ppp.2.t
	desc = ppp.2.d
	picture = GFX_report_event_bulgarian_soldiers
	is_triggered_only = yes
	option = {
		name = ppp.2.a
		ai_chance = {
			factor = 99
		}
		if = {
			limit = {
				has_idea = great_depression
			}
			remove_ideas = great_depression
		}
		if = {
			limit = {
				has_idea = great_depression_2
			}
			remove_ideas = great_depression_2
		}
		if = {
			limit = {
				has_idea = great_depression_3
			}
			remove_ideas = great_depression_3
		}
		add_ideas = extensive_conscription
		add_ideas = war_economy
		add_war_support = 0.3
		news_event = {
			hours = 6
			id = sss.9
		}
	}
	option = {
		name = ppp.2.b
		ai_chance = {
			factor = 1
		}
	}
}

#葡萄牙的的反应
country_event = {
	id = ppp.3
	title = ppp.3.t
	desc = ppp.3.d
	picture = GFX_report_event_bulgarian_soldiers
	is_triggered_only = yes
	option = {
		name = ppp.3.a
		ai_chance = {
			factor = 95
		}
		FFF = {
			transfer_state = 179
		}
		FFF = {
			transfer_state = 112
		}
		FFF = {
			transfer_state = 181
		}
		FFF = {
			country_event = {
				days = 5
				id = fff.12
			}
		}
		news_event = {
			hours = 6
			id = sss.10
		}
	}
	option = {
		name = ppp.3.b
		ai_chance = {
			factor = 5
		}
		FFF = {
			transfer_state = 540
		}
	}
}

#和德国的谈判
country_event = {
	id = ppp.4
	title = ppp.4.t
	desc = ppp.4.d
	picture = GFX_report_event_bulgarian_soldiers
	is_triggered_only = yes
	option = {
		name = ppp.4.a
		ai_chance = {
			factor = 90
		}
		GER = {
			country_event = {
				days = 5
				id = ppp.5
			}
		}
	}
	option = {
		name = ppp.3.b
		ai_chance = {
			factor = 10
		}
		GER = {
			country_event = {
				days = 5
				id = ppp.6
			}
		}
	}
}

#英国人同意了我们的要求
country_event = {
	id = ppp.5
	title = ppp.5.t
	desc = ppp.5.d
	picture = GFX_report_event_hitler_parade
	is_triggered_only = yes
	option = {
		name = ppp.5.a
		ENG = {
			country_event = {
				days = 3
				id = ppp.7
			}
		}
		GER = {
			transfer_state = 85
		}
		GER = {
			transfer_state = 86
		}
		GER = {
			transfer_state = 87
		}
		GER = {
			transfer_state = 762
		}
		GER = {
			transfer_state = 28
		}
		add_state_core = 28
		add_state_core = 85
		add_state_core = 86
		add_state_core = 87
		news_event = {
			hours = 6
			id = sss.16
		}
		GER = {
			annex_country = {
				target = AUS
				transfer_troops = yes
			}
			every_state = {
				limit = {
					is_core_of = AUS
				}
				PREV = {
					ADD_STATE_CORE = PREV
				}
			}
		}
	}
}

#英国人拒绝了我们的要求
country_event = {
	id = ppp.6
	title = ppp.6.t
	desc = ppp.6.d
	picture = GFX_report_event_hitler_parade
	is_triggered_only = yes
	option = {
		name = ppp.6.a
		GER = {
			create_wargoal = {
				type = puppet_wargoal_focus
				target = ENG
			}
		}
	}
}

#德国的参战
country_event = {
	id = ppp.7
	title = ppp.7.t
	desc = ppp.7.d
	picture = GFX_report_event_hitler_parade
	is_triggered_only = yes
	option = {
		name = ppp.7.a
		GER = {
			add_to_war = {
				targeted_alliance = PREV
				enemy = FFF
				hostility_reason = asked_to_join
			}
		}
	}
}

#德国的建议-FRA
country_event = {
	id = ppp.8
	title = ppp.8.t
	desc = ppp.8.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.8.a
		ai_chance = {
			factor = 20
			modifier = {
				factor = 100
				FROM = {
					is_ai = no
				}
			}
		}
		custom_effect_tooltip = GAME_OVER_TT
		GER = { country_event = { id = ppp.59 days = 1 random_days = 6 } }
	}
	option = {
		name = ppp.8.b
		ai_chance = {
			factor = 20
		}
		GER = { country_event = { id = ppp.60 days = 1 random_days = 6 } }
	}
}

#德国的建议-法国同意了
country_event = {
	id = ppp.59
	title = ppp.59.t
	desc = ppp.59.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.59.a
		FRA = {
			every_character = {
				set_nationality = GER
			}
		}
		annex_country = { target = FRA transfer_troops = yes }
		every_state = {
			limit = {
				is_core_of = FRA
			}
			add_core_of = GER
		}
	}
}

#德国的建议-法国拒绝了
country_event = {
	id = ppp.60
	title = ppp.60.t
	desc = ppp.60.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.60.a
		GER = {
			add_to_faction = FRA
		}
	}
}

#德国的建议-ITA
country_event = {
	id = ppp.9
	title = ppp.9.t
	desc = ppp.9.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.9.a
		ai_chance = {
			factor = 20
			modifier = {
				factor = 100
				FROM = {
					is_ai = no
				}
			}
		}
		custom_effect_tooltip = GAME_OVER_TT
		GER = { country_event = { id = ppp.61 days = 1 random_days = 6 } }
	}
	option = {
		name = ppp.9.b
		ai_chance = {
			factor = 10
		}
		GER = { country_event = { id = ppp.62 days = 1 random_days = 6 } }
	}
}

#德国的建议-意大利同意了
country_event = {
	id = ppp.61
	title = ppp.61.t
	desc = ppp.61.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.61.a
		ITA = {
			every_character = {
				set_nationality = GER
			}
		}
		annex_country = { target = ITA transfer_troops = yes }
		every_state = {
			limit = {
				is_core_of = ITA
			}
			add_core_of = GER
		}
	}
}

#德国的建议-意大利拒绝了
country_event = {
	id = ppp.62
	title = ppp.62.t
	desc = ppp.62.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.62.a
		GER = {
			add_to_faction = ITA
		}
	}
}

#德国的建议-ENG
country_event = {
	id = ppp.10
	title = ppp.10.t
	desc = ppp.10.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.10.a
		ai_chance = {
			factor = 0
			modifier = {
				factor = 10
				GER = {
					is_ai = no
				}
			}
		}
		custom_effect_tooltip = GAME_OVER_TT
		GER = { country_event = { id = ppp.63 days = 1 random_days = 6 } }
	}
	option = {
		name = ppp.10.b
		ai_chance = {
			factor = 50
		}
		GER = { country_event = { id = ppp.64 days = 1 random_days = 6 } }
	}
}

#德国的建议-英国同意了
country_event = {
	id = ppp.63
	title = ppp.63.t
	desc = ppp.63.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.63.a
		ENG = {
			every_character = {
				set_nationality = GER
			}
		}
		annex_country = { target = ENG transfer_troops = yes }
		every_state = {
			limit = {
				is_core_of = ENG
			}
			add_core_of = GER
		}
	}
}

#德国的建议-英国拒绝了
country_event = {
	id = ppp.64
	title = ppp.64.t
	desc = ppp.64.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.64.a
		GER = {
			add_to_faction = ENG
		}
	}
}

#德国的建议-BEL
country_event = {
	id = ppp.11
	title = ppp.11.t
	desc = ppp.11.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.11.a
		ai_chance = {
			factor = 90
			modifier = {
				factor = 100
				GER = {
					is_ai = no
				}
			}
		}
		GER = {
			annex_country = {
				target = BEL
				transfer_troops = yes
			}
		}
		every_state = {
			limit = {
				is_core_of = BEL
			}
			add_core_of = GER
		}
		GER = { country_event = { id = ppp.65 days = 1 random_days = 6 } }
	}
	option = {
		name = ppp.11.b
		ai_chance = {
			factor = 0
		}
		GER = {
			add_to_faction = BEL
		}
	}
}

#德国的建议-HOL
country_event = {
	id = ppp.12
	title = ppp.12.t
	desc = ppp.12.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.12.a
		ai_chance = {
			factor = 90
			modifier = {
				factor = 100
				GER = {
					is_ai = no
				}
			}
		}
		GER = {
			annex_country = {
				target = HOL
				transfer_troops = yes
			}
		}
		every_state = {
			limit = {
				is_core_of = HOL
			}
			add_core_of = GER
		}
		GER = { country_event = { id = ppp.65 days = 1 random_days = 6 } }
	}
	option = {
		name = ppp.12.b
		ai_chance = {
			factor = 0
		}
		GER = {
			add_to_faction = HOL
		}
	}
}

#德国的建议-POL
country_event = {
	id = ppp.13
	title = ppp.13.t
	desc = ppp.13.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.13.a
		ai_chance = {
			factor = 90
			modifier = {
				factor = 100
				GER = {
					is_ai = no
				}
			}
		}
		GER = {
			annex_country = {
				target = POL
				transfer_troops = yes
			}
		}
		every_state = {
			limit = {
				is_core_of = POL
			}
			add_core_of = GER
		}
		GER = { country_event = { id = ppp.65 days = 1 random_days = 6 } }
	}
	option = {
		name = ppp.13.b
		ai_chance = {
			factor = 10
		}
		GER = {
			add_to_faction = POL
		}
	}
}

#德国的建议-CZE
country_event = {
	id = ppp.14
	title = ppp.14.t
	desc = ppp.14.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.14.a
		ai_chance = {
			factor = 90
			modifier = {
				factor = 100
				GER = {
					is_ai = no
				}
			}
		}
		GER = {
			annex_country = {
				target = CZE
				transfer_troops = yes
			}
		}
		every_state = {
			limit = {
				is_core_of = CZE
			}
			add_core_of = GER
		}
		GER = { country_event = { id = ppp.65 days = 1 random_days = 6 } }
	}
	option = {
		name = ppp.14.b
		ai_chance = {
			factor = 0
		}
		GER = {
			add_to_faction = CZE
		}
	}
}

#德国的建议-YUG
country_event = {
	id = ppp.15
	title = ppp.15.t
	desc = ppp.15.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.15.a
		ai_chance = {
			factor = 60
			modifier = {
				factor = 100
				GER = {
					is_ai = no
				}
			}
		}
		GER = {
			annex_country = {
				target = YUG
				transfer_troops = yes
			}
		}
		every_state = {
			limit = {
				is_core_of = YUG
			}
			add_core_of = GER
		}
		GER = { country_event = { id = ppp.65 days = 1 random_days = 6 } }
	}
	option = {
		name = ppp.15.b
		ai_chance = {
			factor = 20
		}
		GER = {
			add_to_faction = YUG
		}
	}
}

#德国的建议-ROM
country_event = {
	id = ppp.16
	title = ppp.16.t
	desc = ppp.16.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.16.a
		ai_chance = {
			factor = 90
			modifier = {
				factor = 100
				GER = {
					is_ai = no
				}
			}
		}
		GER = {
			annex_country = {
				target = ROM
				transfer_troops = yes
			}
		}
		every_state = {
			limit = {
				is_core_of = ROM
			}
			add_core_of = GER
		}
		GER = { country_event = { id = ppp.65 days = 1 random_days = 6 } }
	}
	option = {
		name = ppp.16.b
		ai_chance = {
			factor = 10
		}
		GER = {
			add_to_faction = ROM
		}
	}
}

#德国的建议-DEN
country_event = {
	id = ppp.17
	title = ppp.17.t
	desc = ppp.17.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.17.a
		ai_chance = {
			factor = 90
			modifier = {
				factor = 100
				GER = {
					is_ai = no
				}
			}
		}
		GER = {
			annex_country = {
				target = DEN
				transfer_troops = yes
			}
		}
		every_state = {
			limit = {
				is_core_of = DEN
			}
			add_core_of = GER
		}
		GER = { country_event = { id = ppp.65 days = 1 random_days = 6 } }
	}
	option = {
		name = ppp.17.b
		ai_chance = {
			factor = 5
		}
		GER = {
			add_to_faction = DEN
		}
	}
}

#德国的建议-NOR
country_event = {
	id = ppp.18
	title = ppp.18.t
	desc = ppp.18.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.18.a
		ai_chance = {
			factor = 90
			modifier = {
				factor = 100
				GER = {
					is_ai = no
				}
			}
		}
		GER = {
			annex_country = {
				target = NOR
				transfer_troops = yes
			}
		}
		every_state = {
			limit = {
				is_core_of = NOR
			}
			add_core_of = GER
		}
		GER = { country_event = { id = ppp.65 days = 1 random_days = 6 } }
	}
	option = {
		name = ppp.18.b
		ai_chance = {
			factor = 5
		}
		GER = {
			add_to_faction = NOR
		}
	}
}

#德国的建议-SWE
country_event = {
	id = ppp.19
	title = ppp.19.t
	desc = ppp.19.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.19.a
		ai_chance = {
			factor = 90
			modifier = {
				factor = 100
				GER = {
					is_ai = no
				}
			}
		}
		GER = {
			annex_country = {
				target = SWE
				transfer_troops = yes
			}
		}
		every_state = {
			limit = {
				is_core_of = SWE
			}
			add_core_of = GER
		}
		GER = { country_event = { id = ppp.65 days = 1 random_days = 6 } }
	}
	option = {
		name = ppp.19.b
		ai_chance = {
			factor = 5
		}
		GER = {
			add_to_faction = SWE
		}
	}
}

#德国的建议-BUL
country_event = {
	id = ppp.20
	title = ppp.20.t
	desc = ppp.20.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.20.a
		ai_chance = {
			factor = 90
			modifier = {
				factor = 100
				GER = {
					is_ai = no
				}
			}
		}
		GER = {
			annex_country = {
				target = BUL
				transfer_troops = yes
			}
		}
		every_state = {
			limit = {
				is_core_of = BUL
			}
			add_core_of = GER
		}
		GER = { country_event = { id = ppp.65 days = 1 random_days = 6 } }
	}
	option = {
		name = ppp.20.b
		ai_chance = {
			factor = 10
		}
		GER = {
			add_to_faction = BUL
		}
	}
}

#德国的建议-GRE
country_event = {
	id = ppp.21
	title = ppp.21.t
	desc = ppp.21.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.21.a
		ai_chance = {
			factor = 90
			modifier = {
				factor = 100
				GER = {
					is_ai = no
				}
			}
		}
		GER = {
			annex_country = {
				target = GRE
				transfer_troops = yes
			}
		}
		every_state = {
			limit = {
				is_core_of = GRE
			}
			add_core_of = GER
		}
		GER = { country_event = { id = ppp.65 days = 1 random_days = 6 } }
	}
	option = {
		name = ppp.21.b
		ai_chance = {
			factor = 5
		}
		GER = {
			add_to_faction = GRE
		}
	}
}

#德国的建议-HUN
country_event = {
	id = ppp.22
	title = ppp.22.t
	desc = ppp.22.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.22.a
		ai_chance = {
			factor = 90
			modifier = {
				factor = 100
				GER = {
					is_ai = no
				}
			}
		}
		GER = {
			annex_country = {
				target = HUN
				transfer_troops = yes
			}
		}
		every_state = {
			limit = {
				is_core_of = HUN
			}
			add_core_of = GER
		}
		GER = { country_event = { id = ppp.65 days = 1 random_days = 6 } }
	}
	option = {
		name = ppp.22.b
		ai_chance = {
			factor = 0
		}
		GER = {
			add_to_faction = HUN
		}
	}
}

#德国的建议-ICE
country_event = {
	id = ppp.23
	title = ppp.23.t
	desc = ppp.23.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.23.a
		ai_chance = {
			factor = 90
			modifier = {
				factor = 100
				GER = {
					is_ai = no
				}
			}
		}
		GER = {
			annex_country = {
				target = ICE
				transfer_troops = yes
			}
		}
		every_state = {
			limit = {
				is_core_of = ICE
			}
			add_core_of = GER
		}
		GER = { country_event = { id = ppp.65 days = 1 random_days = 6 } }
	}
	option = {
		name = ppp.23.b
		ai_chance = {
			factor = 0
		}
		GER = {
			add_to_faction = ICE
		}
	}
}

#德国的建议-LUX
country_event = {
	id = ppp.24
	title = ppp.24.t
	desc = ppp.24.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.24.a
		ai_chance = {
			factor = 90
			modifier = {
				factor = 100
				GER = {
					is_ai = no
				}
			}
		}
		GER = {
			annex_country = {
				target = LUX
				transfer_troops = yes
			}
		}
		every_state = {
			limit = {
				is_core_of = LUX
			}
			add_core_of = GER
		}
		GER = { country_event = { id = ppp.65 days = 1 random_days = 6 } }
	}
	option = {
		name = ppp.24.b
		ai_chance = {
			factor = 10
		}
		GER = {
			add_to_faction = LUX
		}
	}
}

#德国的建议-SWI
country_event = {
	id = ppp.66
	title = ppp.66.t
	desc = ppp.66.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.66.a
		ai_chance = {
			factor = 90
			modifier = {
				factor = 100
				GER = {
					is_ai = no
				}
			}
		}
		GER = {
			annex_country = {
				target = SWI
				transfer_troops = yes
			}
		}
		every_state = {
			limit = {
				is_core_of = SWI
			}
			add_core_of = GER
		}
		GER = { country_event = { id = ppp.65 days = 1 random_days = 6 } }
	}
	option = {
		name = ppp.66.b
		ai_chance = {
			factor = 10
		}
		GER = {
			add_to_faction = SWI
		}
	}
}

#德国的建议-ALB
country_event = {
	id = ppp.67
	title = ppp.67.t
	desc = ppp.67.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.67.a
		ai_chance = {
			factor = 90
			modifier = {
				factor = 100
				GER = {
					is_ai = no
				}
			}
		}
		GER = {
			annex_country = {
				target = ALB
				transfer_troops = yes
			}
		}
		every_state = {
			limit = {
				is_core_of = ALB
			}
			add_core_of = GER
		}
		GER = { country_event = { id = ppp.65 days = 1 random_days = 6 } }
	}
	option = {
		name = ppp.67.b
		ai_chance = {
			factor = 10
		}
		GER = {
			add_to_faction = ALB
		}
	}
}


#德国的建议-小国们同意了
country_event = {
	id = ppp.65
	title = ppp.65.t
	desc = ppp.65.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	fire_only_once = yes
	option = {
		name = ppp.65.a
	}
}

#考察团准备好了
country_event = {
	id = ppp.25
	title = ppp.25.t
	desc = ppp.25.d
	picture = GFX_report_event_destroyers
	is_triggered_only = yes
	option = {
		name = ppp.25.a
		hidden_effect = {
			random_list = {
				65 = {
					country_event = {
						days = 10
						id = ppp.26
					}
				}
				40 = {
					country_event = {
						days = 10
						id = ppp.27
					}
				}
			}
			news_event = {
				hours = 16
				id = sss.21
			}
		}
	}
}

#考察团灭亡
country_event = {
	id = ppp.26
	title = ppp.26.t
	desc = ppp.26.d
	picture = GFX_report_event_generic_communist_congress
	is_triggered_only = yes
	immediate = {
		set_global_flag = kaocatuanmiewang		#考擦团灭亡
	}
	option = {
		name = ppp.26.a
		add_stability = -0.1
		news_event = {
			hours = 6
			id = sss.22
		}
	}
}

#考察团的成果
country_event = {
	id = ppp.27
	title = ppp.27.t
	desc = ppp.27.d
	picture = GFX_report_event_generic_army
	is_triggered_only = yes
	immediate = {
		set_global_flag = sangshi_shiti		#美国考察团带回来了丧尸的尸体
	}
	option = {
		name = ppp.27.a
		add_war_support = 0.15
		news_event = {
			hours = 6
			id = sss.23
		}
	}
}

#再派考察团
country_event = {
	id = ppp.28
	title = ppp.28.t
	desc = ppp.28.d
	picture = GFX_report_event_generic_communist_congress
	is_triggered_only = yes
	option = {
		name = ppp.28.a
		hidden_effect = {
			country_event = {
				days = 10
				id = ppp.27
			}
			news_event = {
				hours = 16
				id = sss.24
			}
		}
	}
}

#德国的提议
country_event = {
	id = ppp.29
	title = ppp.29.t
	desc = ppp.29.d
	picture = GFX_report_event_lithuania_army
	is_triggered_only = yes
	option = {
		name = ppp.29.a
		ai_chance = {
			factor = 90
		}
		GER = {
			add_to_faction = ENG
			add_to_faction = FRA
			add_to_faction = ITA
			add_to_faction = CZE
			add_to_faction = BEL
			add_to_faction = POL
			add_to_faction = YUG
			add_to_faction = ROM
			add_to_faction = DEN
			add_to_faction = NOR
			add_to_faction = SWE
			add_to_faction = BUL
			add_to_faction = GRE
			add_to_faction = HUN
			add_to_faction = HOL
			add_to_faction = LUX
			add_to_faction = IRE
		}
		news_event = {
			hours = 160
			id = sss.20
		}
	}
	option = {
		ai_chance = {
			factor = 0
		}
		name = ppp.29.b
	}
}

#德国的提议-USA
country_event = {
	id = ppp.30
	title = ppp.30.t
	desc = ppp.30.d
	picture = GFX_report_event_canada_patriation
	is_triggered_only = yes
	option = {
		name = ppp.30.a
		ai_chance = {
			factor = 90
		}
		GER = {
			country_event = {
				days = 10
				id = ppp.31
			}
		}
	}
	option = {
		ai_chance = {
			factor = 10
		}
		name = ppp.30.b
		GER = {
			country_event = {
				days = 10
				id = ppp.32
			}
		}
	}
}

#德国的提议-USA-成功
country_event = {
	id = ppp.31
	title = ppp.31.t
	desc = ppp.31.d
	picture = GFX_report_event_generic_research_lab
	is_triggered_only = yes
	option = {
		name = ppp.31.a
		GER = {
			add_ideas = yanjioucezhonu
		}
		FFF = {
			add_ideas = renlei
		}
		GGG = {
			add_ideas = renlei
		}
		HHH = {
			add_ideas = renlei
		}
		news_event = {
			hours = 16
			id = sss.28
		}
	}
}

#德国的提议-USA-失败
country_event = {
	id = ppp.32
	title = ppp.32.t
	desc = ppp.32.d
	picture = GFX_report_event_generic_research_lab
	is_triggered_only = yes
	option = {
		name = ppp.32.a
	}
}

#美国想成立北美战区
country_event = {
	id = ppp.33
	title = ppp.33.t
	desc = ppp.33.d
	picture = GFX_report_event_burma_road
	is_triggered_only = yes
	option = {
		name = ppp.33.a
		CAN = {
			country_event = {
				days = 3
				id = ppp.34
			}
		}
	}
}

#英国要求我们与美国合并成立北美战区
country_event = {
	id = ppp.34
	title = ppp.34.t
	desc = ppp.34.d
	picture = GFX_report_event_canada_patriation
	is_triggered_only = yes
	option = {
		name = ppp.34.a
		ai_chance = {
			factor = 90
		}
		CAN = {
			every_character = {
				set_nationality = USA
			}
		}
		USA = {
			annex_country = {
				target = CAN
				transfer_troops = yes
			}
			set_cosmetic_tag = USA_beimei
		}
		every_state = {
			limit = {
				is_core_of = CAN
			}
			add_core_of = USA
		}
		USA = {
			hidden_effect = {
				load_oob = "USA_can"
			}
			custom_effect_tooltip = USA_can
		}
		USA = {
			transfer_state = 332
		}
		USA = {
			transfer_state = 331
		}
		news_event = {
			hours = 6
			id = sss.29
		}
	}
	option = {
		ai_chance = {
			factor = 0
		}
		name = ppp.34.b
		USA = {
			country_event = {
				days = 10
				id = ppp.35
			}
		}
	}
}

#加拿大不同意
country_event = {
	id = ppp.35
	title = ppp.35.t
	desc = ppp.35.d
	picture = GFX_report_event_ast_navy
	is_triggered_only = yes
	option = {
		name = ppp.35.a
	}
}

#我们应该联合起来-JAP
country_event = {
	id = ppp.36
	title = ppp.36.t
	desc = ppp.36.d
	picture = GFX_report_event_japanese_siam_politicians
	is_triggered_only = yes
	fire_only_once = yes
	mean_time_to_happen = {
		days = 0
	}
	option = {
		name = ppp.36.a
		CHI = {
			country_event = {
				days = 5
				id = ppp.37
			}
		}
		create_faction = "yazhou"
		news_event = {
			hours = 6
			id = sss.32
		}
	}
}

#日本的请求
country_event = {
	id = ppp.37
	title = ppp.37.t
	desc = ppp.37.d
	picture = GFX_report_event_china_ledo_road
	is_triggered_only = yes
	option = {
		name = ppp.37.a
		ai_chance = {
			factor = 95
		}
		add_ideas = guojianinju
		set_cosmetic_tag = CHI_yazhou
		JAP = {
			add_to_faction = CHI
		}
		JAP = {
			country_event = {
				days = 5
				id = ppp.38
			}
		}
		PRC = {
			country_event = {
				days = 10
				id = ppp.40
			}
		}
		SHX = {
			country_event = {
				days = 10
				id = ppp.41
			}
		}
		GXC = {
			country_event = {
				days = 10
				id = ppp.42
			}
		}
		SIK = {
			country_event = {
				days = 10
				id = ppp.43
			}
		}
		TIB = {
			country_event = {
				days = 10
				id = ppp.44
			}
		}
		XSM = {
			country_event = {
				days = 10
				id = ppp.45
			}
		}
		YUN = {
			country_event = {
				days = 10
				id = ppp.46
			}
		}
	}
	option = {
		name = ppp.37.b
		ai_chance = {
			factor = 5
		}
		JAP = {
			country_event = {
				days = 10
				id = ppp.39
			}
		}
	}
}

#中国拒绝了
country_event = {
	id = ppp.39
	title = ppp.39.t
	desc = ppp.39.d
	picture = GFX_report_event_japanese_siam_politicians
	is_triggered_only = yes
	option = {
		name = ppp.39.a
	}
}

#中国接受了
country_event = {
	id = ppp.38
	title = ppp.38.t
	desc = ppp.38.d
	picture = GFX_report_event_japanese_siam_politicians
	is_triggered_only = yes
	option = {
		name = ppp.38.a
		set_cosmetic_tag = JAP_yazhou
		CHI = {
			annex_country = {
				target = MAN
				transfer_troops = yes
			}
		}
		CHI = {
			annex_country = {
				target = MEN
				transfer_troops = yes
			}
		}
		CHI = {
			transfer_state = 609
		}
		CHI = {
			transfer_state = 524
		}
		CHI = {
			transfer_state = 745
		}
		CHI = {
			transfer_state = 326
		}
		CHI = {
			transfer_state = 729
		}
		CHI = {
			transfer_state = 728
		}
		news_event = {
			hours = 6
			id = sss.30
		}
	}
}

#国民政府的通牒PRC
country_event = {
	id = ppp.40
	title = ppp.40.t
	desc = ppp.40.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.40.a
		ai_chance = {
			factor = 100
		}
		CHI = {
			annex_country = {
				target = PRC
				transfer_troops = yes
			}
		}
	}
	option = {
		name = ppp.40.b
		ai_chance = {
			factor = 0
		}
		JAP = {
			add_to_faction = PRC
		}
	}
}

#国民政府的通牒SHX
country_event = {
	id = ppp.41
	title = ppp.41.t
	desc = ppp.41.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.41.a
		ai_chance = {
			factor = 100
		}
		CHI = {
			annex_country = {
				target = SHX
				transfer_troops = yes
			}
		}
	}
	option = {
		name = ppp.41.b
		ai_chance = {
			factor = 0
		}
		JAP = {
			add_to_faction = SHX
		}
	}
}

#国民政府的通牒GXC
country_event = {
	id = ppp.42
	title = ppp.42.t
	desc = ppp.42.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.42.a
		ai_chance = {
			factor = 100
		}
		CHI = {
			annex_country = {
				target = GXC
				transfer_troops = yes
			}
		}
	}
	option = {
		name = ppp.42.b
		ai_chance = {
			factor = 0
		}
		JAP = {
			add_to_faction = GXC
		}
	}
}

#国民政府的通牒SIK
country_event = {
	id = ppp.43
	title = ppp.43.t
	desc = ppp.43.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.43.a
		ai_chance = {
			factor = 100
		}
		CHI = {
			annex_country = {
				target = SIK
				transfer_troops = yes
			}
		}
	}
	option = {
		name = ppp.40.b
		ai_chance = {
			factor = 0
		}
		JAP = {
			add_to_faction = SIK
		}
	}
}

#国民政府的通牒TIB
country_event = {
	id = ppp.44
	title = ppp.44.t
	desc = ppp.44.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.44.a
		ai_chance = {
			factor = 100
		}
		CHI = {
			annex_country = {
				target = TIB
				transfer_troops = yes
			}
		}
	}
	option = {
		name = ppp.44.b
		ai_chance = {
			factor = 0
		}
		JAP = {
			add_to_faction = TIB
		}
	}
}

#国民政府的通牒XSM
country_event = {
	id = ppp.45
	title = ppp.45.t
	desc = ppp.45.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.45.a
		ai_chance = {
			factor = 100
		}
		CHI = {
			annex_country = {
				target = XSM
				transfer_troops = yes
			}
		}
	}
	option = {
		name = ppp.45.b
		ai_chance = {
			factor = 0
		}
		JAP = {
			add_to_faction = XSM
		}
	}
}

#国民政府的通牒YUN
country_event = {
	id = ppp.46
	title = ppp.46.t
	desc = ppp.46.d
	picture = GFX_report_event_fascist_gathering
	is_triggered_only = yes
	option = {
		name = ppp.46.a
		ai_chance = {
			factor = 100
		}
		CHI = {
			annex_country = {
				target = YUN
				transfer_troops = yes
			}
		}
	}
	option = {
		name = ppp.46.b
		ai_chance = {
			factor = 0
		}
		JAP = {
			add_to_faction = YUN
		}
	}
}

#我们应该联合起来-SOV
country_event = {
	id = ppp.47
	title = ppp.47.t
	desc = ppp.47.d
	picture = GFX_report_event_carol_meeting
	is_triggered_only = no
	fire_only_once = yes
	trigger = {
		tag = SOV
		NOT = {
			SOV = {
				is_in_faction_with = GER
			}
		}
		SOV = {
			has_war_with = FFF
		}
	}
	mean_time_to_happen = {
		days = 0
	}
	option = {
		name = ppp.47.a
		create_faction = "zhondon"
		SOV = {
			add_to_faction = TUR
			add_to_faction = AFG
			add_to_faction = FIN
			add_to_faction = EST
			add_to_faction = LAT
			add_to_faction = LIN
			add_to_faction = MON
			add_to_faction = TAN
		}
		news_event = {
			hours = 16
			id = sss.31
		}
	}
}

#法国的抉择
country_event = {
	id = ppp.48
	title = ppp.48.t
	desc = ppp.48.d
	picture = GFX_report_event_japan_army_mountainside
	is_triggered_only = yes
	fire_only_once = yes
	option = {
		name = ppp.48.a
		ai_chance = {
			factor = 10
		}
		FFF = {
			transfer_state = 515
		}
		FFF = {
			transfer_state = 556
		}
		FFF = {
			transfer_state = 272
		}
		FFF = {
			transfer_state = 660
		}
		FFF = {
			transfer_state = 539
		}
		FFF = {
			add_manpower = 1000000
		}
		GGG = {
			add_manpower = 1000000
		}
		news_event = {
			hours = 16
			id = sss.33
		}
	}
	option = {
		name = ppp.48.b
		ai_chance = {
			factor = 90
		}
	}
}

#英国的抉择
country_event = {
	id = ppp.49
	title = ppp.49.t
	desc = ppp.49.d
	picture = GFX_report_event_japan_army_mountainside
	is_triggered_only = yes
	fire_only_once = yes
	option = {
		# Accept demands
		name = ppp.49.a
		ai_chance = {
			factor = 90
		}
		FFF = {
			transfer_state = 542
		}
		FFF = {
			transfer_state = 545
		}
		FFF = {
			transfer_state = 546
		}
		FFF = {
			transfer_state = 547
		}
		FFF = {
			transfer_state = 548
		}
		FFF = {
			transfer_state = 549
		}
		FFF = {
			add_manpower = 1000000
		}
		GGG = {
			add_manpower = 1000000
		}
		news_event = {
			hours = 16
			id = sss.33
		}
	}
	option = {
		# Refuse
		name = ppp.49.b
		ai_chance = {
			factor = 10
		}
	}
}

#是否进行研究遏制丧尸?
country_event = {
	id = ppp.50
	title = ppp.50.t
	desc = ppp.50.d
	picture = GFX_report_event_japan_army_mountainside
	is_triggered_only = yes
	fire_only_once = yes
	option = {
		name = ppp.50.a
		ai_chance = {
			factor = 90
		}
		add_ideas = yanjioucezhonu
		FFF = {
			add_ideas = HZ
		}
		GGG = {
			add_ideas = HZ
		}
		HHH = {
			add_ideas = HZ
		}
	}
	option = {
		name = ppp.50.b
		ai_chance = {
			factor = 10
		}
		add_stability = 0.2
	}
}

#是否进行研究遏制丧尸?
country_event = {
	id = ppp.51
	title = ppp.51.t
	desc = ppp.51.d
	picture = GFX_report_event_japan_army_mountainside
	is_triggered_only = yes
	fire_only_once = yes
	option = {
		name = ppp.51.a
		ai_chance = {
			factor = 10
		}
		add_ideas = yanjioucezhonu
		FFF = {
			add_ideas = HZ
		}
		GGG = {
			add_ideas = HZ
		}
		HHH = {
			add_ideas = HZ
		}
	}
	option = {
		name = ppp.51.b
		ai_chance = {
			factor = 90
		}
		add_stability = 0.2
	}
}

#是否进行研究遏制丧尸？
country_event = {
	id = ppp.52
	title = ppp.52.t
	desc = ppp.52.d
	picture = GFX_report_event_japan_army_mountainside
	is_triggered_only = yes
	fire_only_once = yes
	option = {
		name = ppp.52.a
		ai_chance = {
			factor = 10
		}
		add_ideas = yanjioucezhonu
		FFF = {
			add_ideas = HZ1
		}
		GGG = {
			add_ideas = HZ1
		}
		HHH = {
			add_ideas = HZ1
		}
	}
	option = {
		name = ppp.52.b
		ai_chance = {
			factor = 90
		}
		add_stability = 0.2
	}
}

#是否进行研究遏制丧尸？
country_event = {
	id = ppp.53
	title = ppp.53.t
	desc = ppp.53.d
	picture = GFX_report_event_japan_army_mountainside
	is_triggered_only = yes
	fire_only_once = yes
	option = {
		name = ppp.53.a
		ai_chance = {
			factor = 90
		}
		add_ideas = yanjioucezhonu
		FFF = {
			add_ideas = HZ1
		}
		GGG = {
			add_ideas = HZ1
		}
		HHH = {
			add_ideas = HZ1
		}
	}
	option = {
		name = ppp.53.b
		ai_chance = {
			factor = 10
		}
		add_stability = 0.2
	}
}

#是否进行研究遏制丧尸？
country_event = {
	id = ppp.54
	title = ppp.54.t
	desc = ppp.54.d
	picture = GFX_report_event_japan_army_mountainside
	is_triggered_only = yes
	fire_only_once = yes
	option = {
		name = ppp.54.a
		ai_chance = {
			factor = 90
		}
		add_ideas = yanjioucezhonu
		FFF = {
			add_ideas = HZ2
		}
		GGG = {
			add_ideas = HZ2
		}
		HHH = {
			add_ideas = HZ2
		}
	}
	option = {
		name = ppp.54.b
		ai_chance = {
			factor = 10
		}
		add_stability = 0.2
	}
}

#是否进行研究遏制丧尸？
country_event = {
	id = ppp.55
	title = ppp.55.t
	desc = ppp.55.d
	picture = GFX_report_event_japan_army_mountainside
	is_triggered_only = yes
	fire_only_once = yes
	option = {
		name = ppp.55.a
		ai_chance = {
			factor = 10
		}
		add_ideas = yanjioucezhonu
		FFF = {
			add_ideas = HZ2
		}
		GGG = {
			add_ideas = HZ2
		}
		HHH = {
			add_ideas = HZ2
		}
	}
	option = {
		name = ppp.55.b
		ai_chance = {
			factor = 90
		}
		add_stability = 0.2
	}
}

#是否进行研究遏制丧尸？
country_event = {
	id = ppp.56
	title = ppp.56.t
	desc = ppp.56.d
	picture = GFX_report_event_japan_army_mountainside
	is_triggered_only = yes
	fire_only_once = yes
	option = {
		name = ppp.56.a
		ai_chance = {
			factor = 10
		}
		add_ideas = yanjioucezhonu
		FFF = {
			add_ideas = HZ
		}
		GGG = {
			add_ideas = HZ
		}
		HHH = {
			add_ideas = HZ
		}
	}
	option = {
		name = ppp.56.b
		ai_chance = {
			factor = 90
		}
		add_stability = 0.2
	}
}

#苏联的方案会议
country_event = {
	id = ppp.57
	title = ppp.57.t
	desc = ppp.57.d
	picture = GFX_report_event_soviet_finnish_pact
	is_triggered_only = yes
	option = {
		name = ppp.57.a
		ai_chance = {
			factor = 100
		}
		add_ideas = xuanzezmianjipo
	}
	option = {
		name = ppp.57.b
		ai_chance = {
			factor = 0
		}
		add_ideas = dongyonkexue
	}
}

#我们已经找到了限制丧尸感染的办法
country_event = {
	id = ppp.58
	title = ppp.58.t
	desc = ppp.58.d
	picture = GFX_report_event_soviet_finnish_pact
	is_triggered_only = yes
	option = {
		name = ppp.58.a
		ai_chance = {
			factor = 100
		}
		add_ideas = wrdtxy
		FFF = {
			add_ideas = rlxz
		}
		GGG = {
			add_ideas = rlxz
		}
		HHH = {
			add_ideas = rlxz
		}
		news_event = {
			hours = 16
			id = sss.42
		}
	}
}
