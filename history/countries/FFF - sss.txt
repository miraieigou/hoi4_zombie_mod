﻿capital = 495
oob = "FFF_1936"
set_research_slots = 0
set_convoys = 5000
set_technology = {
	infantry_weapons = 1
	gw_artillery = 1
	tech_support = 1
	#land_doctrine
	mass_assault = 1
	pocket_defence = 1
	defence_in_depth = 1
	large_front_operations = 1
	deep_operations = 1
	operational_concentration = 1
	vast_offensives = 1
	breakthrough_priority = 1
	mechanized_wave = 1
	continuous_offensive = 1
	peoples_army = 1
	human_infantry_offensive = 1
	large_front_offensive = 1
	human_wave_offensive = 1
	guerilla_warfare = 1
}

add_ideas = {
	gr
	closed_economy
	tot_economic_mobilisation
	No_thought
	Insane
	Meat
}

set_politics = {
	ruling_party = NOthought
	last_election = "1930.11.9"
	election_frequency = 1000
	elections_allowed = no
}

set_popularities = {
	neutrality = 0
	fascism = 0
	democratic = 0
	communism = 0
	NOthought = 100
}

set_stability = 1
set_war_support = 1
#leaders
recruit_character = fff_Zomie_leader_sangshi
recruit_character = FFF_Zomie_leader_despotism
recruit_character = FFF_Zomie_leader_leninism
recruit_character = FFF_Zomie_leader_liberalism
recruit_character = FFF_Zomie_leader_nazism
#advisors
recruit_character = FFF_ZomieAdvisor_Construction
recruit_character = FFF_ZomieAdvisor_Production
recruit_character = FFF_ZomieAdvisor_Army
recruit_character = FFF_ZomieAdvisor_InfantryChief
recruit_character = FFF_InfantryHighCommand_Infanrty
recruit_character = FFF_InfantryHighCommand_Xp
recruit_character = FFF_InfantryHighCommand_AP